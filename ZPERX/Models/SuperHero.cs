﻿using System.ComponentModel.DataAnnotations;

namespace ZPERX.Models
{
    public class SuperHero
    {
        [Required]
        public int? Id { get; set; }
        [Required]
        public string? Name { get; set; } 
        [Required]
        public string? FirstName { get; set; }
        [Required]
        public string? LastName { get; set; } 
        [Required]
        public string? Place { get; set; }

    }

    public class GetAllHeroResponse
    {
        public bool IsSuccess { get; set; }
        public string? Message { get; set; }
        public List<SuperHero>? data { get; set; }

    }

    public class GetSingleHeroResponse
    {
        public bool IsSuccess { get; set; }
        public string? Message { get; set; }
        public SuperHero? data { get; set; }
    }
}
