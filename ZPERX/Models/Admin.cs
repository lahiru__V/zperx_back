﻿using System.ComponentModel.DataAnnotations;

namespace ZPERX.Models
{
    public class Admin
    {
        [Required]
        public int? Id { get; set; }

        [Required]
        public string? UserName { get; set; }

        [Required]
        public string? Password { get; set; }        
    }
}
