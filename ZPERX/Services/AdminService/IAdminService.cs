﻿using ZPERX.Models;

namespace ZPERX.Services.AdminService
{
    public interface IAdminService
    {
        Task<Response> CreateAdmin(Admin admin);

    }
}
