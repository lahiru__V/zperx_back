﻿using ZPERX.Data;
using ZPERX.Models;

namespace ZPERX.Services.AdminService
{
    public class AdminService : IAdminService
    {
        private readonly DataContext _context;

        public AdminService(DataContext context)
        {
            _context = context;
        }        
        public async Task<Response> CreateAdmin(Admin admin)
        {
            Response response = new Response();
            response.IsSuccess = true;
            response.Message = "Admin Created Successfuly !";
            try
            {
                _context.Admin.Add(admin);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "Exception Occurs : " + ex.Message;
            }
            return response;
        }             
    }
}

