﻿using ZPERX.Models;

namespace ZPERX.Services.SuperHeroService
{
    public interface ISuperHeroService
    {
        Task<GetAllHeroResponse> GetAllHeroes();
        Task<GetSingleHeroResponse> GetSingleHero(int id);
        Task<Response> AddHero(SuperHero hero);
        Task<Response> UpdateHero(SuperHero request);
        Task<Response> DeleteHero(int id);
    }
}
