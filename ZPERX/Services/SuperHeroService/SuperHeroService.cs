﻿using ZPERX.Data;
using ZPERX.Models;

namespace ZPERX.Services.SuperHeroService
{
    public class SuperHeroService : ISuperHeroService
    {
        private readonly DataContext _context;

        public SuperHeroService(DataContext context)
        {
            _context = context;
        }

        //public async Task<List<SuperHero>> AddHero(SuperHero hero)
        //{
        //    try
        //    {
        //        _context.SuperHeroes.Add(hero);
        //        await _context.SaveChangesAsync();
        //        return await _context.SuperHeroes.ToListAsync();
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine($"Error adding hero: {ex.Message}");
        //        throw;
        //    }
        //}  
        public async Task<Response> AddHero(SuperHero hero)
        {
            Response response = new Response();
            response.IsSuccess = true;
            response.Message = "Hero Successfuly Created";
            try
            {               
                _context.SuperHeroes.Add(hero);
                await _context.SaveChangesAsync();                
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "Exception Occurs : " + ex.Message;
            }
            return response;
        }

        //public async Task<List<SuperHero>?> DeleteHero(int id)
        //{
        //    var hero = await _context.SuperHeroes.FindAsync(id);
        //    if (hero is null)
        //        return null;

        //    _context.SuperHeroes.Remove(hero);
        //    await _context.SaveChangesAsync();

        //    return await _context.SuperHeroes.ToListAsync();
        //}

        public async Task<Response> DeleteHero(int id)
        {
            Response response = new Response();
            response.IsSuccess = true;
            response.Message = "Hero Delete Successfully";

            try
            {
                var hero = await _context.SuperHeroes.FindAsync(id);

                if (hero != null)
                {
                    _context.SuperHeroes.Remove(hero);
                    await _context.SaveChangesAsync();
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Hero not found";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "Exception Occurs: " + ex.Message;
            }

            return response;
        }


        //public async Task<List<SuperHero>> GetAllHeroes()
        //{
        //    var heroes = await _context.SuperHeroes.ToListAsync();
        //    return heroes;
        //}

        public async Task<GetAllHeroResponse> GetAllHeroes()
        {
            GetAllHeroResponse response = new GetAllHeroResponse();
            response.IsSuccess = true;
            response.Message = "Data Fetch Successfully";
            try
            {
                response.data = new List<SuperHero>();
                response.data = await _context.SuperHeroes.ToListAsync();
                if (response.data.Count == 0)
                {
                    response.Message = "No Record Found";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "Exception Occurs : " + ex.Message;
            }
            return response;

        }

        public async Task<GetSingleHeroResponse> GetSingleHero(int id)
        {
            GetSingleHeroResponse response = new GetSingleHeroResponse();

            try
            {                
                response.data = await _context.SuperHeroes.FindAsync(id);
                if (response.data == null)
                {
                    response.Message = "Invalid ID";
                }
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "Exception Occurs : " + ex.Message;
            }
            return response;
        }

        //public async Task<List<SuperHero>?> UpdateHero(SuperHero request)
        //{
        //    var hero = await _context.SuperHeroes.FindAsync(request.Id);
        //    if (hero is null)
        //        return null;

        //    hero.FirstName = request.FirstName;
        //    hero.LastName = request.LastName;
        //    hero.Name = request.Name;
        //    hero.Place = request.Place;

        //    await _context.SaveChangesAsync();

        //    return await _context.SuperHeroes.ToListAsync();
        //}

        public async Task<Response> UpdateHero(SuperHero request)
        {
            Response response = new Response();
            response.IsSuccess = true;
            response.Message = "Record Update Successfully By ID";
            try
            {
                var hero = await _context.SuperHeroes.FindAsync(request.Id);
                if (hero != null)
                {
                    hero.FirstName = request.FirstName;
                    hero.LastName = request.LastName;
                    hero.Name = request.Name;
                    hero.Place = request.Place;
                    await _context.SaveChangesAsync();
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "Hero not found";
                }
                             
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = "Exception Occurs : " + ex.Message;
            }
            return response;
        }
    }
}

