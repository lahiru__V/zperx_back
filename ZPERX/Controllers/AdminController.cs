﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZPERX.Models;
using ZPERX.Services.AdminService;
using ZPERX.Services.SuperHeroService;

namespace ZPERX.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IAdminService _adminService;

        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
        }

        [HttpPost]
        public async Task<IActionResult> CreateAdmin(Admin admin)
        {
            var result = await _adminService.CreateAdmin(admin);
            return Ok(result);
        }
    }
}
