﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZPERX.Models;
using ZPERX.Services.SuperHeroService;

namespace ZPERX.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperHeroController : ControllerBase
    {
        private readonly ISuperHeroService _superHeroService;

        public SuperHeroController(ISuperHeroService superHeroService)
        {
            _superHeroService = superHeroService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllHeroes()
        {
            GetAllHeroResponse response = new GetAllHeroResponse();
            response = await _superHeroService.GetAllHeroes();            
            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSingleHero(int id)
        {
            GetSingleHeroResponse response = new GetSingleHeroResponse();
            response = await _superHeroService.GetSingleHero(id);           
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> AddHero(SuperHero hero)
        {
            var result = await _superHeroService.AddHero(hero);
            return Ok(result);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateHero(SuperHero request)
        {
            var result = await _superHeroService.UpdateHero(request);           
            return Ok(result);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteHero(int id)
        {
            var result = await _superHeroService.DeleteHero(id);
            if (result is null)
                return NotFound("Hero not found.");

            return Ok(result);
        }
    }
}

